try:
    import asyncio
    import aiohttp
except Exception as e:
    with open("errors.txt", "a") as errorfile:
        errorfile.write(str(e)+'\n')
    print("Import error")
    exit()


class AsyncClient:

    def __init__(self, loop: asyncio.get_event_loop):
        self.session = aiohttp.ClientSession(loop=loop)

    async def close(self):
        """
        Closes the client's session
        """
        self.session.close()

    async def request(self, urls: [str, 'iterable']) -> dict:
        """
        Returns a dict {url1: response, url2: response, ... } from singular url or iterable of urls}
        """
        if type(urls) is str:
            async with self.session.get(urls) as response:
                return {urls: await response.read()}
        else:
            #responses is a set with all the response coroutines at idx [0].
            responses = await asyncio.wait([self.request(url) for url in urls])
            #coro_responses is the set of responses that we called. we reset responses to a dict to use later
            coro_responses = responses[0]
            responses = {}
            #this is where we get the result and add them to the responses dict
            for coro_response in coro_responses:
                #coro_response.result() is the dict {url: response}.
                responses.update(coro_response.result())
            return responses
